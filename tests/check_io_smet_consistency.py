import os
import sys
import configparser
import numpy as np
import pandas as pd

import awio
from awsmet import smet_parser

def compare_smets(config, vstation_id):
    
    print(f"[i]  Checking consistency of input/output SMET files for one single vstation:")
    print(f"     {vstation_id}")
    # Input nowcast
    in_smet_nowcast = smet_parser.SMETParser(f"{os.path.join(config.get('Paths', '_smet_files_dir_nowcast'), str(vstation_id))}.smet")
    in_df_nowcast = in_smet_nowcast.df()
    in_df_nowcast['TA'] = in_df_nowcast['TA'] - 273.15
    # Input forecast
    forecast_file = f"{os.path.join(config.get('Paths', '_smet_files_dir_forecast'), str(vstation_id))}.smet"
    if os.path.exists(forecast_file):
        in_smet_forecast = smet_parser.SMETParser()
        in_df_forecast = in_smet_forecast.df()
        in_df_forecast['TA'] = in_df_forecast['TA'] - 273.15
    # Output 
    out_smet = smet_parser.SMETParser(f"{os.path.join(config.get('Paths', '_snp_output_dir'), str(vstation_id))}.smet")
    out_df = out_smet.df()

    result = {}
    quantiles = [.01, .25, .50, .75, .99]
    threshold = 10e-4
    for col in in_df_nowcast.columns:
        if col != 'PSUM':
            qus = np.nanquantile(in_df_nowcast[col] - out_df[col], quantiles)
            if (qus > threshold).any():
                result[col] = qus
        else:
            qus = np.nanquantile(in_df_nowcast['PSUM'] - (out_df['MS_Snow'] + out_df['MS_Rain']), quantiles)
            # Slightly adjusted threshold for PSUM to account for small precip phase discrepancies
            if qus[0] > 2 or qus[-1] > 2 or (qus[1:-1] > threshold).any():
                result[col] = qus
        
    result_forecast = {}
    if 'in_df_forecast' in locals():
        for col in in_df_forecast.columns:
            if col != 'PSUM':
                qus = np.nanquantile(in_df_nowcast[col] - out_df[col], quantiles)
                if (qus > threshold).any():
                    result_forecast[col] = qus
            else:
                qus = np.nanquantile(in_df_nowcast['PSUM'] - (out_df['MS_Snow'] + out_df['MS_Rain']), quantiles)
                if qus[0] > 2 or qus[-1] > 2 or (qus[1:-1] > threshold).any():
                    result_forecast[col] = qus

    if result or result_forecast:
        print("[w]")
        
        if result:
            print("     Differences Input(nowcast) - Output:")
            header = f"     {'Field':<15} {'Q1':<10} {'Q25':<10} {'Q50':<10} {'Q75':<10} {'Q99':<10}"
            print("-" * len(header))
            print(header)
            for col, qus in result.items():
                print(f"     {col:<15} {qus[0]:<10.5f} {qus[1]:<10.5f} {qus[2]:<10.5f} {qus[3]:<10.5f} {qus[4]:<10.5f}")
        
        if result_forecast:
            print("\n     Differences Input(forecast) - Output:")
            header = f"     {'Field':<15} {'Q1':<10} {'Q25':<10} {'Q50':<10} {'Q75':<10} {'Q99':<10}"
            print("-" * len(header))
            print(header)
            for col, qus in result_forecast.items():
                print(f"     {col:<15} {qus[0]:<10.5f} {qus[1]:<10.5f} {qus[2]:<10.5f} {qus[3]:<10.5f} {qus[4]:<10.5f}")

        print("\nIf you see differences in the interquartile range (Q25--Q75), it suggests that MeteoIO's filters are regularly kicking in. Check your input dataset!\n" +
              "Large differences in the edge quantiles (Q1, Q99) may indicate timesteps with severe outliers. That's why MeteoIO's filters are useful, but it doesn't hurt to cross-check your dataset.\n")
    else:
        print("     SMET files are consistent.")


if __name__ == "__main__":

    if len(sys.argv) >= 2:
        domain = sys.argv[1]
    if len(sys.argv) > 3:
        sys.exit("[E] Synopsis: python3 check_io_smet_consistency.py domain [vstation_id]")
    
    
    config = awio.get_config(domain, 'gridded-chain')

    if len(sys.argv) >= 3:
        vstation_id = sys.argv[2]
    else:
        df_vstations = pd.read_csv(config.get('Paths', '_vstations_csv_file_runtime'))
        vstation_id = f"VIR{df_vstations.loc[df_vstations['error'] == 0, 'vstation'][0]}"

    compare_smets(config, vstation_id)

from datetime import datetime, timedelta, date
import time
import glob
import shutil
import os
import sys
import subprocess
import configparser
import warnings
# warnings.simplefilter(action='ignore', category=FutureWarning)
import multiprocessing

import numpy as np
import pandas as pd

import awio
import nwp_provider as awnwp
from awset import get, getattribute, exists, DEFAULT_DOMAIN_PATH, aenv
from awsmet.smet_parser import SMETParser

from profsnow.snowpack.SNEngine import SNEngine

from snowpacktools.snowpro import snowpro
from snowpacktools.avapro import avapro
from snowpacktools.aggregatepro import gridded as gag
from snowpacktools.rproplots import rproplots
from qmah.processing import process_vstations as pv
from qmah.datetags import datetags
import pdb

date_format       = '%Y-%m-%d'
datetime_format   = '%Y-%m-%dT%H:%M'
datetime_format_2 = '%Y-%m-%dT%H:%M:%S'
debug_mode_dict = {
    '0': 'run all',
    '1': 'only postprocessing',
    '2': 'skip NWP update and SMET file generation and start simulating as quickly as possible',
    '3': 'only initializing of config_runtime_domain',
    '4': 'initializing config, clearing and creating file structure, generating vstations_csv file',
}

def run_domain(domain="default"):
    """runs gridded SNOWPACK simulations driven by NWP model data.
    
    This function takes the domain (configuration) file and runs gridded SNOWPACK simulations based
    on NWP model data. It can simulate flat field and slopes for different aspects for a selection 
    of grid points from the NWP model (check options for generate_vstations_csv.py). It supports an operational
    mode that only simulates the new timestamps and initializes SNOWPACK with a saved snow profile (.sno-file)
    to save computation time. Deactivating the operational mode results in simulating again from the season
    start.
    Defining the periods for nowcast/hintcast and forecast can be tricky to understand. Follow the corresponding
    print statements for tracing the logic in the code. Simply simulating the whole season without the operational mode
    is trivial because simulations always start again at season start. However, in operational mode the code basically
    checks the timestamps of the NETCDF files nwp_stacked.nc and nwp_fc.nc and the timestamp of the saved snow profile.
    If date_opera, e.g., is set back to an earlier timestamp, the simulations need to start again. On the other hand,
    setting date_opera a few days into the future should keep the operational mode alive by just simulating a longer
    nowcast period and saving a .sno file after nwp_stacked.nc.

    Parameters
    ----------
    domain: str
        domain descriptor to be appended to directory names and for fetching configuration file

    """

    print(f"[i]  Starting gridded SNOWPACK simulations at {datetime.now()}.")
    config = _initialize_config(domain)
    config = _get_date_opera(config)
    if config.get('General','debug_mode') != "0":
        print(f"[i]  Debug mode {config.get('General','debug_mode')} activated: {debug_mode_dict[config.get('General','debug_mode')]}")

    with open(config.get("Paths","_ini_runtime_domain"), 'w') as configfile:
        config.write(configfile)
    if config.get('General','debug_mode') == "3":
        print("[i]  Done, existing now.")
        sys.exit(0)

    """Generate vstations"""
    os.makedirs(config.get('Paths','_vstations_csv_dir'), exist_ok=True)
    if not os.path.exists(config.get('Paths','_vstations_csv_file')):
        returnCode = subprocess.call([sys.executable, config.get('Paths','_generate_vstations_script'), domain])
        if returnCode != 0:
            raise ValueError("Could not generate vstations csv file")
    else:
        print(f"[i]  vstations.csv-file exists already: {config.get('Paths','_vstations_csv_file')}")
    if config.get('General','debug_mode') == "4":
        print("[i]  Done, exiting now.")
        sys.exit(0)

    df_vstations = pd.read_csv(config.get('Paths', '_vstations_csv_file'))
    config['General']["ntasks"] = str(len(df_vstations))

    with open(config.get("Paths","_ini_runtime_domain"), 'w') as configfile:
        config.write(configfile)

    ## - Weather data: Generating NETCDF files (and SMET files) - ##
    ncpus = int(get(["gridded-chain", "run", "ncpus"], domain, cfgpath=config.get('Paths', '_domain_path'), throw=True))
    if config.get('General','debug_mode') in ["1","2"]:
        print("[i]  Skipping NWP update and skipping SMET file generation.")
        sys.stdout.flush()
        ds_nwp  = awnwp.open_dataset(domain, 'gridded-chain', 'nowcast', update_dataset=False, aux_config=config)
        end_nowcast = datetime.strftime(pd.Timestamp(ds_nwp.time[-1].values), format=datetime_format)
        ds_nwp.close()
        if config.getboolean('General', 'run_forecast'):
            ds_nwp_fc  = awnwp.open_dataset(domain, 'gridded-chain', 'forecast', update_dataset=False, aux_config=config)
            end_forecast = datetime.strftime(min(pd.Timestamp(ds_nwp_fc.time[-1].values), datetime.strptime(config.get('General', 'season_end'), date_format)), datetime_format)
            ds_nwp_fc.close()
    else:
        print("[i]  Preparing weather data.")
        sys.stdout.flush()
        shutil.rmtree(config.get('Paths','_smet_files_dir_nowcast'), ignore_errors=True)
        shutil.rmtree(config.get('Paths','_smet_files_dir_forecast'), ignore_errors=True)

        ds_nwp  = awnwp.open_dataset(domain, 'gridded-chain', 'nowcast', update_dataset=True, aux_config=config, prefer_nc_over_db=True)
        meteo_vars = get(config.get('Paths', f'_xml_path_meteo_nowcast').split('/') + ['variables'], domain, config.get('Paths', '_domain_path'))
        print("[i]  Generating SMET files for nowcast.")
        awnwp.smet_generator.generate_smets_for_pois(ds_nwp, df_vstations, config.get('Paths','_smet_files_dir_nowcast'),
                                                meteo_vars, ncpus=ncpus, semidistributed=(config.get('General', 'distribution')=='semi-distributed'))
        end_nowcast = datetime.strftime(pd.Timestamp(ds_nwp.time[-1].values), format=datetime_format)
        ds_nwp.close()
        if config.getboolean('General', 'run_forecast'):
            ds_nwp_fc  = awnwp.open_dataset(domain, 'gridded-chain', 'forecast', update_dataset=True, aux_config=config)
            meteo_vars = get(config.get('Paths', f'_xml_path_meteo_forecast').split('/') + ['variables'], domain, config.get('Paths', '_domain_path'))
            print("[i]  Generating SMET files for forecast.")
            awnwp.smet_generator.generate_smets_for_pois(ds_nwp_fc, df_vstations, config.get('Paths','_smet_files_dir_forecast'),
                                                    meteo_vars, ncpus=ncpus, semidistributed=(config.get('General', 'distribution')=='semi-distributed'),
                                                    ds_nwp_prior=ds_nwp)
            end_forecast = datetime.strftime(min(pd.Timestamp(ds_nwp_fc.time[-1].values), datetime.strptime(config.get('General', 'season_end'), date_format)), datetime_format)
            ds_nwp_fc.close()
    
    if config.getboolean('General', 'resume') and (len(glob.glob(os.path.join(config.get('Paths','_sno_nowcast_dir'), "*.sno"))) > 0):
        begin_nowcast = SMETParser.get_header(sorted(glob.glob(os.path.join(config.get('Paths','_sno_nowcast_dir'), "*.sno")))[0])["ProfileDate"]
        print(f"[i]  Resuming from .sno-files activated. Starting with profiles from {begin_nowcast}.")
    else:
        print(f"[i]  Resuming from .sno-files NOT activated. Starting at season start {config.get('General','season_start')}")

    ## - SNOWPACK simulations - ##
    if config.get('General','debug_mode') != "1":
        SNEngine.run_awsome_snowpack(domain, "gridded-chain") 

        ## - Check (one) vstation for I/O SMET file consistency - ##
        ## Potential inconsistencies could mean that MeteoIO's filters kick in.
        ## We want to inform users so that they can catch situations where, for example, 
        ## their entire input data set gets filtered and re-generated.
        # TODO: Make this function general for smet_folder and output folder and move test to engine to profsnow/tests
        # if n_vstations_error < df_vstations.shape[0]:
        #     subprocess.call([sys.executable, os.path.expanduser('~snowpack/gridded-chain/tests/check_io_smet_consistency.py'), 
        #                     config.get('General','domain'), 
        #                     f"VIR{df_vstations.loc[df_vstations['error'] == 0, 'vstation'][0]}"])
        
        ## - Save runtime config for postprocessing - ##
        with open(config.get("Paths","_ini_runtime_domain"), 'w') as configfile:
            config.write(configfile)
    else:
        print("[i]  Skipping snow cover simulations. Continue with postprocessing.")

    ## - POSTPROCESSING - ##
    if getattribute(["gridded-chain", "output", "qmah-geojson"], "enabled", domain, cfgpath=config.get('Paths','_domain_path')) == "true":
        config.read(config.get("Paths","_ini_runtime_domain"))
        ## - Identify timestamps for generating geojson files (e.g. for last week and forecast) - ##  
        ts_start = datetime.strptime(config.get('General','date_opera'), date_format) - timedelta(days=5)
        # ts_start = datetime.strptime(config.get('General','begin_nowcast'), datetime_format)
        if config.getboolean('General','run_forecast'):
            ts_end = datetime.strptime(end_forecast, datetime_format)
        else:
            ts_end = datetime.strptime(end_nowcast, datetime_format)
        
        if not config.getboolean("General","resume"):
            shutil.rmtree(config.get('Paths','_qmah_geojson_dir'), ignore_errors=True)
        os.makedirs(config.get('Paths','_qmah_geojson_dir'), exist_ok=True)
        _call_qmah_and_upload_geojson(config, ts_start, ts_end)
    
    """Generate representative/average profiles for each region--band--aspect combination"""
    if config.getboolean('Aggregate','aggregate_profiles'):
        try:
            gag.aggregate(domain=domain)
        except:
            pass

    """Plot profiles using snowpacktools.rproplots"""
    if config.getboolean("Rproplots", "enabled"):
        df_vstations = pd.read_csv(config.get('Paths', '_vstations_csv_file'))
        pro_files = [f"VIR{pro}.pro" for pro in df_vstations.loc[df_vstations['error'] == 0, 'vstation']]
        _call_rproplots_and_upload(config, pro_files, end_nowcast)

    """Call AVAPRO for determining avalanche problems"""
    # Use ini file from snowpacktools package here similar to Flo
    # rscript path = pkg_resources. resource filename ('snowpacktools', 'aggregatepro/aggregate gridded


def _initialize_config(domain):
    """Load relevant parameters from domain file and define file/folder paths
    
    _initialize_config loads the domain file, processes the configuration and defines relevant paths
    for folders and scripts to be executed. Debug options for development are integrated in
    this function.
    
    """
    
    config             = configparser.ConfigParser()
    config.optionxform = str
    gridded_chain_dir  = os.path.expanduser("~snowpack/gridded-chain")
    spath              = awio.filetools.get_scriptpath(__file__)[:-1] # remove last /
    _domain_path       = DEFAULT_DOMAIN_PATH

    os.makedirs(f"{gridded_chain_dir}/data", exist_ok=True)
    os.makedirs(f"{gridded_chain_dir}/input", exist_ok=True)
    os.makedirs(f"{gridded_chain_dir}/output", exist_ok=True)

    """Determine output directories if provided in domain settings"""
    # NOTE: The output directory is only defined here to inform post-processing. 
    # The actual output dir is set in SNEngine.run_awsome_snowpack() using the same logic! 
    if getattribute(["gridded-chain", "output", "directory"], "enabled", domain, cfgpath=_domain_path) == "true":
        _output_dir = get(["gridded-chain", "output", "directory"], domain, cfgpath=_domain_path, throw=True)
    else:
        _output_dir = os.path.expanduser(f"~snowpack/gridded-chain/output")
    if getattribute(["gridded-chain", "output", "snp-directory"], "enabled", domain, cfgpath=_domain_path) == "true":
        _snp_output_dir = get(["gridded-chain", "output", "snp-directory"], domain, cfgpath=_domain_path, throw=True)
    else:
        _snp_output_dir = f'{_output_dir}/snp-{domain}'


    # TODO: Several hardcoded paths are still not updated!
    config['Paths'] = {'_domain_path' :                 _domain_path,
                       '_private_urls_ini_path':        aenv("AWSOME_SECRETS"),
                       # '_cwd_':                         f'~snowpack/gridded-chain',
                       '_ini_runtime_domain':           os.path.join(f"{gridded_chain_dir}/input", "runtime_" + domain + ".ini"),
                       'upload_script_path':            os.path.join(aenv("AWSOME_BASE"), 'visualization/upload/upload.py'),
                       '_snowpro_ini_template_path':    f'{gridded_chain_dir}/input/templates/snowpro_template.ini',
                       '_snowpro_ini_path':             f'{gridded_chain_dir}/input/snowpro.ini',
                       '_avapro_ini_template_path':     f'{gridded_chain_dir}/input/templates/avapro_template.ini',
                       '_avapro_ini_path':              f'{gridded_chain_dir}/input/avapro.ini',
                       '_smet_files_dir_nowcast':       f'{gridded_chain_dir}/data/smet-files/{domain}/nowcast',
                       '_smet_files_dir_forecast':      f'{gridded_chain_dir}/data/smet-files/{domain}/forecast',
                       '_sno_nowcast_dir':              f'{gridded_chain_dir}/data/snow-nowcast/{domain}',
                       '_vstations_csv_dir':            f'{gridded_chain_dir}/data/vstations',
                       '_vstations_csv_file':           f'{gridded_chain_dir}/data/vstations/vstations-{domain}.csv',
                       '_output_dir':                   f'{_output_dir}',
                       '_snp_output_dir':               f'{_snp_output_dir}',
                       '_qmah_geojson_dir':             f'{_output_dir}/qmah-geojson-{domain}',
                       '_rproplots_dir':                f'{_output_dir}/rproplots-{domain}',
                       '_microregions_dir':             get(["domain", "shape", "file"], domain, cfgpath=_domain_path, throw=True),
                       '_dem_dir':                      get(["domain", "dem", "file"], domain, cfgpath=_domain_path, throw=True),
                       '_generate_vstations_script':    f'{spath}/vstation_generator/generate_vstations_csv.py',
                       '_snp_ini_file':                 get(['gridded-chain', 'snowpack', 'snp_ini_file'], domain, cfgpath=_domain_path, throw=True) \
                                                        if getattribute(['gridded-chain', 'snowpack', 'snp_ini_file'], 'enabled', domain, cfgpath=_domain_path) == "true" \
                                                        else 'default'}
    
    config['General'] = {'season_start':         get(["gridded-chain", "run", "season_start"], domain, cfgpath=_domain_path, throw=True),             
                         'season_end':           get(["gridded-chain", "run", "season_end"], domain, cfgpath=_domain_path, throw=True),                  
                         'date_opera':           get(["gridded-chain", "run", "date_opera"], domain, cfgpath=_domain_path, throw=True),
                         'nslopes':              get(["gridded-chain", "snowpack", "nslopes"], domain, cfgpath=_domain_path, throw=True),
                         'debug_mode':           get(['gridded-chain', 'run', 'debug_mode'], domain, cfgpath=_domain_path, throw=True) \
                                                   if getattribute(['gridded-chain', 'run', 'debug_mode'], 'enabled', domain, cfgpath=_domain_path) == "true" \
                                                   else '0',
                         'distribution':         str(getattribute(['gridded-chain', 'run', 'distribution'], 'type', domain, cfgpath=_domain_path)),
                         'ncpus':                get(["gridded-chain", "run", "ncpus"], domain, cfgpath=_domain_path, throw=True) \
                                                   if getattribute(["gridded-chain", "run", "ncpus"], "enabled", domain, cfgpath=_domain_path) == "true" \
                                                   else str(int(multiprocessing.cpu_count()-2)),
                          'domain':               domain,
                          'toolchain':            'gridded-chain'
    }
    config['SpatialFilter'] = {
        'wkt'           : str(get(['gridded-chain', 'run', 'spatial_filter', 'wkt'], domain, cfgpath=_domain_path)),
        'wkt_crs'      : str(get(['gridded-chain', 'run', 'spatial_filter', 'wkt_crs'], domain, cfgpath=_domain_path)),
        'wkt_buffer'    : str(get(['gridded-chain', 'run', 'spatial_filter', 'wkt_buffer'], domain, cfgpath=_domain_path))
    }
    
    """Simulation modes"""
    if getattribute(["gridded-chain", "run", "resume"], "enabled", domain, cfgpath=_domain_path) == "true":
        config['General']['resume']  = "TRUE"
    else:
        config['General']['resume']  = "FALSE"
    if exists(["gridded-chain", "meteo", "source"], domain, _domain_path):
        config['Paths']['_xml_path_meteo_nowcast'] = 'gridded-chain/meteo'
        config['Paths']['_xml_path_meteo_forecast'] = 'gridded-chain/meteo'
        config['General']['run_forecast'] = "TRUE"
    elif exists(["gridded-chain", "meteo", "nowcast", "source"], domain, _domain_path):
        config['Paths']['_xml_path_meteo_nowcast'] = 'gridded-chain/meteo/nowcast'
        if getattribute(["gridded-chain", "meteo", "forecast"], 'enabled', domain, cfgpath=_domain_path) == "true":
            config['General']['run_forecast'] = "TRUE"
            config['Paths']['_xml_path_meteo_forecast'] = 'gridded-chain/meteo/forecast'
        else:
            config['General']['run_forecast'] = "FALSE"
    if getattribute(["gridded-chain", "run", "nvstations_max"], "enabled", domain, cfgpath=_domain_path) == "true":
        config['General']['nvstations_max'] = get(["gridded-chain", "run", "nvstations_max"], domain, cfgpath=_domain_path, throw=True)
    
    """Profile aggregation"""
    if getattribute(["gridded-chain", "output", "aggregatepro"], "enabled", domain, cfgpath=_domain_path) == "true":
        config["Aggregate"] = {'aggregate_profiles' :       'TRUE', 
                               'daily_time':                get(["gridded-chain", "output", "aggregatepro", "daily_time"], domain, cfgpath=_domain_path, throw=True),
                               'initialize_from':           get(["gridded-chain", "output", "aggregatepro", "initialize_from"], domain, cfgpath=_domain_path, throw=True),
                               }
    else:
        config["Aggregate"] = {'aggregate_profiles' :       'FALSE'}

    """QMAH geojsons"""
    if getattribute(["gridded-chain", "output", "qmah-geojson"], "enabled", domain, cfgpath=_domain_path) == "true":
        config["QMAH"] = {'enabled':            'TRUE',
                          'resolution':         get(["gridded-chain", "output", "qmah-geojson", "resolution"], domain, cfgpath=_domain_path, throw=True),
                          'datetags_enabled':   'TRUE' \
                                                  if getattribute(["gridded-chain", "output", "qmah-geojson", "datetags"], "enabled", domain, cfgpath=_domain_path) == "true" \
                                                  else 'FALSE'
                          }
        if config.getboolean("QMAH", 'datetags_enabled'):
            config["QMAH"]['datetags_by'] = get(["gridded-chain", "output", "qmah-geojson", "datetags", "by"], domain, cfgpath=_domain_path, throw=True)
        stab_indices = get(["gridded-chain", "output", "qmah-geojson", "indices"], domain, cfgpath=_domain_path, throw=False)
        if stab_indices is not None:
            config["QMAH"]['indices'] = stab_indices

    if getattribute(["gridded-chain", "output", "rproplots"], "enabled", domain, cfgpath=_domain_path) == "true":
        config["Rproplots"] = {'enabled':       'TRUE',
                               'daily_time':    get(["gridded-chain", "output", "rproplots", "daily_time"], domain, cfgpath=_domain_path, throw=True)
        }
    else:
        config["Rproplots"] = {'enabled':       'FALSE'}

    """Expand absolute (user) paths"""
    config = awio.expanduser(config)

    return config


def _get_date_opera(config):
    """Get date for (virtual) operation"""

    if config.get('General', 'date_opera') == 'TODAY':
        date_opera = date.today()
    else:
        date_opera = datetime.strptime(config.get('General', 'date_opera'), date_format).date()
    date_season_end = datetime.strptime(config.get('General', 'season_end'), date_format).date()
    if date_opera <= date_season_end:
        config['General']['date_opera'] = datetime.strftime(date_opera, date_format)
    else:
        config['General']['date_opera'] = datetime.strftime(date_season_end, date_format)
    return config


def _call_qmah_and_upload_geojson(config: object, ts_start: datetime, ts_end: datetime):

    print("[i]  Generating GEOJSON files for dashboards with QMAH...")
    stime = time.time()
    
    if config.getboolean("QMAH", "datetags_enabled"):
        print("[i]  Not Calculating datetags (yet -- coming soon)...")
        os.makedirs(config.get('Paths','_datetags_json_dir'), exist_ok=True)
        datetags_json_path = f"{config.get('Paths','_datetags_json_dir')}/datetags-{config.get('General', 'domain')}.json"
        try:
            pass
            # TODO:
            datetag_dict = datetags.postprocess_for_datetags()
            datetags_success = True
        except:
            pass
    # call qmah.processing.process_vstations:
    pv_kwargs = {
        'dt':     timedelta(hours=config.getint('QMAH','resolution')),
        'ncpus':  config.getint('General', 'ncpus')
    }
    if config.has_option("QMAH", "indices"):
        pv_kwargs['indices'] = config.get("QMAH", "indices").split(" ")
    if 'datetag_dict' in locals():
        pv_kwargs['datetag_dict'] = datetag_dict

    pv.process_vstations(config.get('General', 'domain'), config.get('Paths', '_vstations_csv_file'), 
                         config.get('Paths', '_qmah_geojson_dir'), ts_start, ts_end, **pv_kwargs)

    print(f"[i]  Writing GEOJSON files completed in {awio.iso_timediff(stime)} hours.")
    if exists(["gridded-chain", "output", "upload"], domain, cfgpath=config.get('Paths','_domain_path')):
        files = glob.glob(f"{config['Paths']['_qmah_geojson_dir']}/*.json")
        host  = get(["gridded-chain", "output", "upload", "host"], domain, cfgpath=config.get('Paths','_domain_path'))
        link  = get(["gridded-chain", "output", "upload", "link"], domain, cfgpath=config.get('Paths','_domain_path'))
        os.makedirs(host, exist_ok=True)
        print(f"[i]  Uploading folder: {config['Paths']['_qmah_geojson_dir']} to {host} ...")
        for file in files:
            subprocess.call([sys.executable, config.get('Paths','upload_script_path'), file, host])
        print(f"[i]  Uploaded files are available at: {link}")


def _call_rproplots_and_upload(config: object, pro_file_basenames: list[str], end_nowcast: datetime):

    print("[i]  Plotting profiles (R scripts) and uploading to dashboard...")
    stime = time.time()
    pro_files = [os.path.join(config.get('Paths', '_snp_output_dir'), pro) for pro in pro_file_basenames]
    rproplots.plot_profiles_R(pro_files, config.get('Paths', '_rproplots_dir'), end_nowcast, ncpus=config.getint('General', 'ncpus'))
    
    print(f"[i]  Plotting profiles completed in {awio.iso_timediff(stime)} hours.")
    if exists(["gridded-chain", "output", "upload"], domain, cfgpath=config.get('Paths','_domain_path')):
        files = glob.glob(f"{config['Paths']['_rproplots_dir']}/*.json")
        host  = get(["gridded-chain", "output", "upload", "host"], domain, cfgpath=config.get('Paths','_domain_path'))
        link  = get(["gridded-chain", "output", "upload", "link"], domain, cfgpath=config.get('Paths','_domain_path'))
        os.makedirs(host, exist_ok=True)
        print(f"[i]  Uploading folder: {config['Paths']['_rproplots_dir']} to {host} ...")
        for file in files:
            subprocess.call([sys.executable, config.get('Paths','upload_script_path'), file, host])
        print(f"[i]  Uploaded files are available at: {link}")


if __name__ == "__main__":
    if len(sys.argv) > 2:
        sys.exit("[E] Synopsis: python3 run_gridded_chain.py [domain] [>> ~opera/logs/snowpack.log 2>&1]")
    if len(sys.argv) >= 2:
        domain = sys.argv[1]
        run_domain(domain)
    else:
        run_domain()
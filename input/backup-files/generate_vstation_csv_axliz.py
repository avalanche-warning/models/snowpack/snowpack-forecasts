#! /usr/bin/python3
################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import os
import numpy as np
import pandas as pd
import xarray as xr

from shapely.geometry import Point, shape
import fiona  # Library for reading shapefiles

def generate_vstations_csv():
    elevation_min = 1700
    region = "AT-07-14-04"
    _csv_file_path = "./input/vstations-" + region + ".csv"
    """Load nwp model data or grid"""
    _nwp_nc_file_path = os.path.join("data", "arome-inca", "nwp_stacked.nc") ### HARDCODED ###
    ds = xr.open_dataset(_nwp_nc_file_path)
    ds = ds.sel(time=ds.time[0])
    # _nwp_dem_file_path          = os.path.join("data", "snp-ready-nc-files", "nwp_dem.nc")
    # _nwp_snp_ready_nc_file_path = os.path.join("data", "snp-ready-nc-files", "nwp_stacked_cf16.nc")
    # ds     = xr.open_dataset(_nwp_snp_ready_nc_file_path)
    # ds_dem = xr.open_dataset(_nwp_dem_file_path)

    """Load Shapefile"""
    geojson_path = "./data/shape-files/micro-regions/AT-07_micro-regions.geojson.json"
    # with fiona.open(shapefile_path, 'r') as shp:
    #     polygon = shape(shp[0]['geometry'])
    with fiona.open(geojson_path, 'r') as src:
        for ii in range(0,len(src)):
            if src[ii]['properties']['id'] == region:
                print("[i]  Generating csv file with vstations for micro region {}: {}".format(ii,region))
                polygon = shape(src[ii]['geometry'])
                break
                
    """Iterate over easting and northing"""
    points_easting  = []
    points_northing = []
    for easting in ds.x.values:
        for northing in ds.y.values:
            lat = ds["lat"].sel(x=easting,y=northing)
            lon = ds["lon"].sel(x=easting,y=northing)

            if Point(lon,lat).within(polygon): # Point(longitude, latitude)
                if ds['HGT_surface'].sel(x=easting,y=northing).values >= elevation_min:
                    points_easting.append(easting)
                    points_northing.append(northing)
    
    """Create Pandas dataframe (rows: vstations, columns: attributes of each station)"""
    columns      = ["vstation", "easting", "northing", "lon", "lat", "elev", "i", "j", "aspect", "band", "region"]
    aspects      = ["A","A1","A2","A3","A4"]
    aspect_names = ["flat","north","east","south","west"]
    vid          = 100001
    for easting,northing in zip(points_easting,points_northing):
        elev    = int(ds['HGT_surface'].sel(x=easting,y=northing).values)
        if elev >= 2500:
            band = 'above_treeline'
        else:
            band = 'below_treeline'

        i   = np.where(ds.x.values==easting)[0][0]
        j   = np.where(ds.y.values==northing)[0][0]
        lat = ds["lat"].sel(x=easting,y=northing).values
        lon = ds["lon"].sel(x=easting,y=northing).values
        # print(i,j,easting,northing,lat,lon)
        for k, expo in enumerate(aspects):
            ### Use only number, so it results in same output of SNP for SMET and NETCDF input
            row = [[str(vid) + expo, easting, northing, lon, lat, elev, i, j, aspect_names[k], band, region]]
            if vid == 100001 and k==0:
                df = pd.DataFrame(row, columns=columns)
            else:
                df = pd.concat([df, pd.DataFrame(row, columns=columns)], ignore_index=True)
        vid += 1
    df.to_csv(_csv_file_path, index=False)
    print("[i]  File {} created with {} virtual stations.".format(_csv_file_path,len(points_easting)))
if __name__ == "__main__":
    generate_vstations_csv()
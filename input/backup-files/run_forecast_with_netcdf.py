#! /usr/bin/python3
################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

# from dataclasses import fields
from datetime import datetime, timedelta, date
import time
import glob
import shutil
import os
import sys
import subprocess
import configparser
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import multiprocessing

import numpy as np
import xarray as xr
import pandas as pd

from awset.domain_settings import get

from awgeo import geosphere_nwp_processor as nwp_processor
from awgeo import netcdf_converter


def run_domain(configuration,domain="default"):
    """Obtain snow profiles and corresponding NWP data, run SNOWPACK on them and upload the results."""

    config = configparser.ConfigParser()
    config.read(configuration)

    """Define relevant parameters from domain and folders"""
    config['Forecast']['SEASON_START']              = get(["forecasts", "run", "season_start"], domain, cfgpath=config.get('Paths','_domain_path'))
    config['Forecast']['SEASON_END']                = get(["forecasts", "run", "season_end"], domain, cfgpath=config.get('Paths','_domain_path'))
    config['Forecast']['DATE_OPERA']                = get(["forecasts", "run", "date_opera"], domain, cfgpath=config.get('Paths','_domain_path'))
    config['Forecast']['NWP_SOURCE']                = get(["forecasts", "meteo", "source"], domain, cfgpath=config.get('Paths','_domain_path'))
    config['Forecast']['SNP_INPUT_MODE']            = get(["forecasts", "meteo", "snp_input_mode"], domain, cfgpath=config.get('Paths','_domain_path'))
    config['Forecast']['NTASKS']                    = "22"
    config['Forecast']['NSLOPES']                   = "5"  # This has to relate to .csv-file - but actual simulations can be controlled via parameter in snp.ini
    config['Forecast']['start_from_snow_status']    = "FALSE"
    config['Paths']['_nwp_dl_dir']                  = os.path.join("./data",config.get('Forecast','NWP_SOURCE'))
    config['Paths']['_snp_ready_nc_files_dir']      = "./data/snp-ready-nc-files"
    config['Paths']['_snp_output_dir']              = "./output/snp"
    config['Paths']['_smet_files_dir']              = "./data/smet-files"
    # config['Paths']['_snp_ini_template_path']       = "./input/templates/snp_io_smet.ini"
    config['Paths']['_snow_status_dir']             = "./data/snow-status"
    config['Paths']['_microregions_dir']            = os.path.join(config.get('Paths','shapefiles_path'),  "eaws-micro-regions")
    config['Paths']['_vstation_ini_file']           = './input/vstations' # './input/vstations.ini'
    config['Paths']['_vstation_template_ini_file']  = './input/templates/vstations_template_netcdf.ini'
    config['Paths']['_vstations_csv_file']          = "./input/vstations-AT-07-14-04.csv"

    """Flexible date for virtual operation"""
    datetime_format = '%Y-%m-%d'
    if config.get('Forecast','DATE_OPERA') == 'TODAY':
        date_opera = date.today()
    else:
        date_opera = datetime.strptime(config.get('Forecast','DATE_OPERA'), datetime_format).date()
    date_season_end = datetime.strptime(config.get('Forecast','SEASON_END'), datetime_format).date()
    if date_opera <= date_season_end:
        config['Forecast']['DATE_OPERA'] = datetime.strftime(date_opera, datetime_format)
    else:
        config['Forecast']['DATE_OPERA'] = datetime.strftime(date_season_end, datetime_format)

    if config.get('Forecast','DEBUG_MODE') == "ZERO":
        print("[i]  Skipping simulations or visualizations of snow profiles. Only reprocess map output.")
    else:
        shutil.rmtree(config.get('Paths','_smet_files_dir'), ignore_errors=True)
        shutil.rmtree(config.get('Paths','_snp_output_dir'), ignore_errors=True)
        os.makedirs(config.get('Paths','_smet_files_dir'), exist_ok=True)
        os.makedirs(config.get('Paths','_snow_status_dir'), exist_ok=True)
        os.makedirs(config.get('Paths','_snp_output_dir'), exist_ok=True)
        os.makedirs(config.get('Paths','_snp_ready_nc_files_dir'), exist_ok=True)

        print("[i]  Checking stacked NWP data.")
        ds_nwp, config = nwp_processor.update_stacked_nwp_file(config)
        returnCode = subprocess.call(["python3", config.get('Paths','update_stacked_nwp_script_path')])
        if returnCode==0:
            print("[i]  NWP data updated.")
        else:
            print("[E]  Updating NWP data not successfull!")
        
        
        if config.get('Forecast', 'SNP_INPUT_MODE') == "SMET":
            print("[i]  SNP_INPUT_MODE: SMET -> Generating SMET files for simulations via Python")   
            df = pd.read_csv(config.get('Paths', '_vstations_csv_file'))
            prof_meta = {}
            for row in range(0,len(df),5):
                prof_meta['filename']    = "VIR" + df["vstation"][row]
                prof_meta['name']        = "VIR" + df["vstation"][row]
                prof_meta['easting']     = df["easting"][row]
                prof_meta['northing']    = df["northing"][row]
                prof_meta['lat']         = df["lat"][row]
                prof_meta['lon']         = df["lon"][row]
                prof_meta['alt']         = df["elev"][row]
                prof_meta['date']        = config.get('Forecast','SEASON_START')
                prof_meta['slope_angle'] = 0
                nwp_processor.load_single_location(config, prof_meta, ds_nwp)
            config['Paths']['_snp_ini_path'] = "./input/templates/snp_io_smet.ini"

        else: # "snp_input_mode" == "NETCDF"
            print("[i]  SNP_INPUT_MODE: NETCDF -> Making NETCDF file snp-ready by adapting NETCDF to CF1.6 convention.")
            config = _prepare_nwp_netcdf_file(config, ds_nwp)
            config['Paths']['_snp_ini_path'] = "./input/templates/snp_io_netcdf.ini"

        
        """Prepare ini-files for SNP simulations"""
        stime = time.time()
        config_snp = configparser.ConfigParser()
        config_snp.read(config.get('Paths','_snp_ini_path'))
        if config.getboolean('Forecast','start_from_snow_status') and os.path.exists(os.path.join(config.get('Paths','_snow_status_dir'), "VIR100001A.sno")):
            config_snp['INPUT']['SNOWPATH'] = config.get('Paths','_snow_status_dir')
            print("[i]  Start from saved snow status (start from latest snow profile)")
        else:
            config['Forecast']['start_from_snow_status'] = "FALSE"
        config = _prepare_vstation_ini(config)

        """SNP runs with multiprocessing"""
        print("[i]  Running SNOWPACK simulations on multiple cpus.")
        print("")
        mio_env = _get_env()
        datetime_format_2 = '%Y-%m-%dT%H:%M'
        DATE_BEGIN_SNP_0  = datetime.strptime(config.get('Forecast','SEASON_START'), datetime_format) + pd.to_timedelta(6, unit='h')
        DATE_END_SNP_0    = pd.Timestamp(ds_nwp.time[-1].values) - pd.to_timedelta(3, unit='h') 
        # DATE_END_SNP_0    = datetime.strptime(config.get('Forecast','DATE_OPERA'), datetime_format) - pd.to_timedelta(2, unit='h') 
        DATE_BEGIN_SNP    = datetime.strftime(DATE_BEGIN_SNP_0, datetime_format_2) 
        DATE_END_SNP      = datetime.strftime(DATE_END_SNP_0, datetime_format_2)

        print("[i]  Number of cpus available: ", multiprocessing.cpu_count())
        print("[i]  Number of tasks distributed: ", config.getint('Forecast','NTASKS'))
        procs = []
        # - Start processes - #
        for i in range(0,config.getint('Forecast','NTASKS')):
            proc = multiprocessing.Process(target=_call_snp, args=(i, config, config_snp, DATE_BEGIN_SNP, DATE_END_SNP, mio_env))
            procs.append(proc)
            proc.start()    
        # - Complete processes - #
        for proc in procs:
            proc.join()
        output_ini_files = glob.glob(config.get("Paths","_snp_output_dir") + '/*NO_EXP.ini')
        output_haz_files = glob.glob(config.get("Paths","_snow_status_dir") + '/*.haz')
        for file in output_ini_files:
            os.remove(file)
        for file in output_haz_files:
            os.remove(file)
        print("[i]  Number of cpus available: ", multiprocessing.cpu_count())
        print("[i]  Number of tasks used: ", config.getint('Forecast','NTASKS'))
        print("[i]  Gridded SNOWPACK simulations using {}-files completed in {}s.".format(config.get('Forecast', 'SNP_INPUT_MODE'), round(time.time()-stime,3)))


    """Generate average profiles for each aspect"""
    ## Determine avalanche problems for average profiles with AVAPRO


    """Call AVAPRO for determining avalanche problems"""
    # Similar to call in profile-forecast

    """Generate Postprocessing-NETCDF with Punstable, Sk38, RTA, avalanche problems"""
    ## Iterate over vstations in csv or PRO-files, use Snowpro for calculating Punstable (or Flo's R-script)
    ## Copy NETCDF-file of NWP model, generate new variables (2D data + time) -> (time,y,x) and append/concatenate
    ## Use indices in csv-file for writing new values to correct grid point in NETCDF


    """Generate leaflet map"""
    map_path = os.path.join(get(["forecasts", "output", "upload", "host"], domain, cfgpath=config.get('Paths','_domain_path')), "forecast_map.html")
    map_link = os.path.join(get(["forecasts", "output", "upload", "link"], domain, cfgpath=config.get('Paths','_domain_path')), "forecast_map.html")
    map_data = {       
        "map_path":          map_path, 
        "microregions":      config.get('Paths','_microregions_dir'),
        "vstations":         config.get('Paths','_vstations_csv_file'),
        "dem_from_nwp":      os.path.join(config.get('Paths','_nwp_dl_dir'), "nwp_stacked.nc"),
        "wrf_data":          "data/wrf/wrfout_d01_2022-10-01_06:00:00" ### HARDCODED ###
    }
    returnCode = subprocess.call(["python3", config.get('Paths','leaflet_map_script_path'), "--map_path", map_data["map_path"], 
                                  "--microregions", map_data["microregions"], "--vstations", map_data['vstations'], "--dem_from_nwp", map_data['dem_from_nwp'],
                                  "--wrf_data", map_data["wrf_data"]])
    if returnCode==0:
        print("[i]  Leaflet map access: {}".format(map_link))
    else:
        print("[E]  Generating leaflet map not successful!")


def _call_snp(i, config, config_snp, DATE_BEGIN_SNP, DATE_END_SNP, mio_env):
    config_snp['GENERAL']['IMPORT_AFTER'] = "vstations" + str(i) + ".ini"  # config.get('Paths', '_vstation_ini_file')
    _snp_ini_file = "./input/snp_io_smet" + str(i) + ".ini"
    # config.get('Paths','_snp_ini_path')
    with open(_snp_ini_file, 'w') as snp_config_file:
        config_snp.write(snp_config_file)
    
    # met_command = "meteoio_timeseries -b " + DATE_BEGIN_SNP + " -e " + DATE_END_SNP + " -c " + "./input/meteoio_nc_test.ini" + " -p"
    # terminalLink = subprocess.Popen(["meteoio_timeseries","-b",DATE_BEGIN_SNP,"-e", DATE_END_SNP, "-c", "./input/meteoio_nc_test.ini"], env=mio_env, stdout=subprocess.PIPE)
    # cmd = ["snowpack","-b",DATE_BEGIN_SNP,"-e", DATE_END_SNP, "-c", config.get('Paths','_snp_ini_path')]
    cmd = ["snowpack","-b",DATE_BEGIN_SNP,"-e", DATE_END_SNP, "-c", _snp_ini_file]
    print('[i]  [Subprocess] ', " ".join(cmd))
    snp_failed = 0
    try:
        terminalLink = subprocess.Popen(cmd, env=mio_env, stdout=subprocess.PIPE) # stderr=subprocess.PIPE
        # ['stdbuf', '-o0']
        output = terminalLink.communicate()[0]
        print(output.decode('UTF-8'))
        if terminalLink.returncode != 0:
            snp_failed = 1
    except:
        print('[E] Calling SNOWPACK failed!')
        snp_failed = 1
    os.remove(_snp_ini_file)
    os.remove(config.get('Paths', '_vstation_ini_file') + str(i) + ".ini")


def _prepare_nwp_netcdf_file(config, ds_nwp):
    """Check if .nc-file already exists and is valid."""
    _nwp_stacked_nc_file_path                      = os.path.join(config.get('Paths','_nwp_dl_dir'), "nwp_stacked_and_fc.nc")
    config['Paths']['_nwp_snp_ready_nc_file_path'] = os.path.join(config.get('Paths','_snp_ready_nc_files_dir'), "nwp_stacked_cf16.nc")
    config['Paths']['_nwp_dem_file_path']          = os.path.join(config.get('Paths','_snp_ready_nc_files_dir'), "nwp_dem.nc")

    stack_start = pd.Timestamp(ds_nwp.time[0].values)
    stack_end   = pd.Timestamp(ds_nwp.time[-1].values)
    
    new_conversion = False
    if os.path.isfile(config.get('Paths','_nwp_snp_ready_nc_file_path')):
        ds_snp_ready    = xr.open_dataset(config.get('Paths','_nwp_snp_ready_nc_file_path'))
        snp_ready_start = pd.Timestamp(ds_snp_ready.time[0].values)
        snp_ready_end   = pd.Timestamp(ds_snp_ready.time[-1].values)

        if snp_ready_start == stack_start and snp_ready_end == stack_end:
            """Valid file"""
            new_conversion = False
        else:
            new_conversion = True
        ds_snp_ready.close()
    else:
        new_conversion = True
        
    if new_conversion:
        print("[i]  New conversion to CF1.6 of nwp NETCDF data from {} to {}".format(stack_start,stack_end))
        ds_nwp.to_netcdf(_nwp_stacked_nc_file_path)
        netcdf_converter.geosphere2cf(_nwp_stacked_nc_file_path, config.get('Paths','_nwp_snp_ready_nc_file_path'), config.get('Paths','_nwp_dem_file_path'), check_meteoio=False)

    return config


def _prepare_vstation_ini(config: object):
    """Update paths (and eventually settings) of SNOWPACK ini file."""
    if config.get('Forecast', 'snp_input_mode') == "SMET":
        _prepare_vstation_ini_smet(config)
    else:
        _prepare_vstation_ini_netcdf(config)
    return config


def _prepare_vstation_ini_smet(config: object):
    """Write vstations from csv file to ini file"""
    
    df                  = pd.read_csv(config.get('Paths', '_vstations_csv_file'))
    nvstations_per_call = int(np.ceil(len(df)/(config.getint('Forecast','NSLOPES') * int(config.get('Forecast','NTASKS')))))
    for i in range(0,int(config.get('Forecast','NTASKS'))):
        lines     = ["[INPUT]\n"]
        sno_lines = ["\n"]
        index = 1
        
        startrow = i * config.getint('Forecast','NSLOPES') * nvstations_per_call
        endrow   = min((i+1) * config.getint('Forecast','NSLOPES') * nvstations_per_call,len(df))
        for row in range(startrow,endrow,config.getint('Forecast','NSLOPES')):
        #for row in range(0,len(df),5):
            ## Stations not needed in ini-file -> takes all files within given folder
            line = "STATION" + str(index) + " = VIR" + df["vstation"][row] + "\n"
            lines.append(line)

            """Sno-files need to be numbered starting with 1"""
            if config.getboolean('Forecast','start_from_snow_status'):
                sno_line = "SNOWFILE" + str(index) + " = VIR" + df["vstation"][row] + "\n"
            else:
                sno_line = "SNOWFILE" + str(index) + " = sno_template\n"
            # sno_line = "SNOWFILE" + str(df["vstation"][row])   + " = sno_temp\n"
            sno_lines.append(sno_line)

            if config.get("Forecast","DEBUG_MODE") == "ONE" and index >= 1:
                break
            index += 1

        lines += sno_lines

        with open(config.get('Paths', '_vstation_ini_file') + str(i) + ".ini", 'w') as new_file:
            new_file.writelines(lines)


def _prepare_vstation_ini_netcdf(config: object):
    """Write vstations from csv file to ini file"""
    df = pd.read_csv(config.get('Paths', '_vstations_csv_file'))

    with open(config.get('Paths', '_vstation_template_ini_file'), 'r') as file:
        lines = file.readlines()

    additional_lines = ["\n"]
    sno_lines = ["\n[INPUT]\n"]
    index = 1
    for row in range(0,len(df),5):
        # line = "VSTATION" + str(df["vstation"][row]) + " = xy(" +  str(df["easting"][row]) + "," + str(df["northing"][row]) + "," +  str(df["elev"][row]) +  ")\n"
        #### METEOIO ERROR (easting and northing switched) ####
        line = "VSTATION" + str(df["vstation"][row]) + " = xy(" +  str(df["northing"][row]) + "," + str(df["easting"][row]) + "," +  str(df["elev"][row]) +  ")\n"
        #### METEOIO ERROR (easting and northing switched) ####
        additional_lines.append(line)
        
        """Sno-files need to be numbered starting with 1"""
        sno_line = "SNOWFILE" + str(index) + " = sno_template\n"
        # sno_line = "SNOWFILE" + str(df["vstation"][row])   + " = sno_temp\n"
        sno_lines.append(sno_line)

        if config.get("Forecast","DEBUG_MODE") == "ONE" and index >= 1:
            break
        index += 1

    lines += additional_lines
    lines += sno_lines

    with open(config.get('Paths', '_vstation_ini_file'), 'w') as new_file:
        new_file.writelines(lines)


def _get_env():
    """Get an environment with simulation software binaries in the PATH."""
    mio_env = os.environ.copy()
    mio_env["PATH"] = mio_env["PATH"] + ":" + os.path.expanduser("~/.local/bin")
    return mio_env


if __name__ == "__main__":
    if len(sys.argv) > 3:
        sys.exit("[E] Synopsis: python3 run_forecast.py [domain] [configuration]")

    """Try changing working directory for Crontab"""
    try:
        os.chdir(os.path.dirname(sys.argv[0]))
    except:
        print('[i] Working directory already set!')

    domain = "default"
    configuration = "input/forecast.ini"
    if len(sys.argv) >= 2:
        domain = sys.argv[1]
    if len(sys.argv) >= 3:
        configuration = sys.argv[2]

    run_domain(configuration, domain)
#! /usr/bin/python3
################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import os
import numpy as np
import pandas as pd
import xarray as xr

def generate_vstations_csv():
    print("[i]  Generating csv file with vstations (for certain region).")
    _csv_file_path              = "./input/vstations_axliz.csv"
    _nwp_dem_file_path          = os.path.join("data", "snp-ready-nc-files", "nwp_dem.nc")
    _nwp_snp_ready_nc_file_path = os.path.join("data", "snp-ready-nc-files", "nwp_stacked_cf16.nc")
    ds     = xr.open_dataset(_nwp_snp_ready_nc_file_path)
    ds_dem = xr.open_dataset(_nwp_dem_file_path)

    """Adjust boundaries here"""
    # SW: xx: 235358.105117 yy: 359029.978226  xx (easting [northing for METEOIO])
    # NO: xx: 252940.730385 yy: 374363.151871
    ds_axliz     = ds.sel(easting=slice(238000,249000),northing=slice(361000,372000))
    ds_dem_axliz = ds_dem.sel(easting=slice(238000,249000),northing=slice(361000,372000))
    dem_filter   = np.where(ds_dem_axliz['surface_altitude'].values>2000,True,False)
    points_lat   = ds_axliz.latitude.values[dem_filter]
    points_lon   = ds_axliz.longitude.values[dem_filter]
    # points_lat = ds_ifs_lizum.lat.values[::n_points,::n_points][dem_filter]
    # points_lon = ds_ifs_lizum.lon.values[::n_points,::n_points][dem_filter]

    """Create Pandas dataframe (rows: vstations, columns: attributes of each station)"""
    columns = ["vstation", "easting", "northing", "lon", "lat", "elev", "i", "j", "expo", "band", "region"]
    # expos = ["F","N","E","S","W"]
    expos = ["E","E1","E2","E3","E4"]
    expo_names = ["flat","north","east","south","west"]

    region  = "AXLIZ"
    vid = 100001
    for lat,lon in zip(points_lat,points_lon):
        ds_temp = ds.where(ds.latitude==lat, drop=True)
        vid_easting  = ds_temp.easting.values[0]
        vid_northing = ds_temp.northing.values[0]
        elev         = int(ds_dem["surface_altitude"].sel(easting=vid_easting,northing=vid_northing).values)
        if elev >= 2500:
            band = 'above_treeline'
        else:
            band = 'below_treeline'
        i = np.where(ds.easting.values==vid_easting)[0][0]
        j = np.where(ds.northing.values==vid_northing)[0][0]
        for k, expo in enumerate(expos):
            row = [[str(vid) + expo, vid_easting, vid_northing, lon, lat, elev, i, j, expo_names[k], band, region]]
            if vid == 100001 and k==0:
                df = pd.DataFrame(row, columns=columns)
            else:
                df = pd.concat([df, pd.DataFrame(row, columns=columns)], ignore_index=True)
        vid += 1
    df.to_csv(_csv_file_path, index=False)
    print("[i]  File {} created.".format(_csv_file_path))
if __name__ == "__main__":
    generate_vstations_csv()
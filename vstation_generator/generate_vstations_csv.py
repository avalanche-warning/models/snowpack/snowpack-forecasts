#! /usr/bin/python3
################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import os
import numpy as np
import pandas as pd
import geopandas as gpd
import xarray as xr
import sys
import configparser
import awio
import pyproj

from shapely.geometry import Polygon, Point
from shapely.wkt import loads as load_wkt
from awset.domain_settings import get, getattribute, get_elev_bands, get_crs

def generate_vstations_csv(domain: str, 
                           config: object=None, 
                           shapefile_path: str=None, 
                           nwp_demfile_path: str=None, 
                           var_name_surface=None,
                           semidistributed_polygons_path=None,
                           subregion_id: str=None,
                           spatial_filter_wkt: str=None,
                           spatial_filter_wkt_crs: str=None,
                           spatial_filter_wkt_buffer: int|float=0,
                           nvstations_max: int=None):
    """
    Generate a CSV file containing virtual station (vstation) information for specified regions,
    derived from geojson files, a digital elevation model (DEM), and the elevation bands specified in 
    the domain.xml config file. This function identifies grid points that meet the criteria of being within
    the desired regions and elevation bands. It then computes relevant attributes for
    each vstation, such as location, elevation, as well as corresponding regions and bands.

    For examples of how the elevation bands can be specified within the domain.xml structure, see the bottom of this file.

    Parameters:
    ----------
    domain : str
        The domain identifier, used to specify the configuration and data paths, as well as locate the domain.xml config file.
    config : configparser.ConfigParser, optional
        The configuration object to specify paths and settings. If None, loaded from default location.
    shapefile_path : str, optional
        Path to the geojson containing region polygons. If None, it's loaded from the domain settings.
    nwp_demfile_path : str, optional
        Path to the NetCDF file containing the digital elevation model (DEM) of the NWP model. If None, it's loaded from the domain settings.
    var_name_surface : str, optional
        Variable name of the surface elevation in the NWP DEM file. If None, it's loaded from the domain settings.
    semidistributed_polygons_path: str, optional
        Path to a geojson file containing the polygon information for a semi-distributed modeling configuration. This is only
        required if the provided NWP DEM is irregularly spaced, and can be retrieved automatically from the domain.xml file.
    subregion_id : str, optional
        Optional subregion identifier to filter which regions to process. If None, all regions in the geojson (shapefile) are processed.
    spatial_filter_wkt : str, optional
        Well-Known Text (WKT) string that defines a spatial filter to apply to the grid points.
    spatial_filter_wkt_crs : str, optional
        The Coordinate Reference System in which the spatial_filter_wkt is defined, parsable by pyproj.CRS (i.e., epsg, proj4/wkt strings)
    spatial_filter_wkt_buffer : int | float, optional
        A buffer distance in the unit of the spatial filter's coordinate system to expand the selection area 
        around the WKT geometry.
    nvstations_max : int, optional
        The maximum number of virtual stations to include in the output CSV. If spatial_filter_wkt is provided,
        the nvstations_max **closest** vstations will be included in the file. For example, to retrieve the one closest grid point to a specific
        location, provide a POINT spatial_filter_wkt, including a buffer and set this argument to 1. To also limit the result to a specific elevation,
        make sure to define and enable the desired elevation band in the domain.xml file. 

    Returns:
    -------
    None
        This function does not return any value. It writes the vstation data directly to a CSV file, 
        under the path given in the config. It contains the data for all the virtual stations that 
        have been identified within the specified regions (geojson) and elevation bands (domain.xml). 
        It looks like 

        "vstation", "easting", "northing", "lon", "lat", "elev", "band", "region_id"

    Raises:
    ------
    Exception
        If the specified shapefile or DEM file paths do not exist, or the path names or not specified in the domain.xml file.
    SystemExit
        If no grid points match the search criteria, terminating the function prematurely.
    
    """
    print("[i]  Generating vstations csv file")
    if not config:
        config = configparser.ConfigParser()
        config_runtime_domain_path = os.path.expanduser(f'~snowpack/gridded-chain/input/runtime_{domain}.ini')
        if os.path.exists(config_runtime_domain_path):
            config.read(config_runtime_domain_path)
        else:
            raise FileNotFoundError("The domain config needs to be initialized before this script can be run. " +
                                    "Enable debug_mode 3 in domain.xml:gridded-chain/run/debug_mode and run_forecast to initialize the config.")
        config = awio.expanduser(config)

    # Get arguments from domain file
    if not nwp_demfile_path:
        nwp_demfile_path = get(['gridded-chain', 'meteo', 'dem', 'nc_file'], domain, config.get('Paths', '_domain_path'), throw=True)
    dem_crs = get_crs(domain, 'crs', toolchain='gridded-chain', fallback_to_netcdf=True, throw=True, cfgpath=config.get('Paths', '_domain_path'))
    ds = xr.open_dataset(nwp_demfile_path)
    grid_coarsening = get(['gridded-chain', 'meteo', 'dem', 'grid_coarsening'], domain, config.get('Paths', '_domain_path'))
    if grid_coarsening is not None:
        ds = ds.thin(int(grid_coarsening))
    if not var_name_surface:
        var_name_surface = get(['gridded-chain', 'meteo', 'dem', 'surface_variable'], domain, config.get('Paths', '_domain_path'), throw=True)
    if not shapefile_path:
        shapefile_path = get(['domain', 'shape', 'file'], domain, config.get('Paths', '_domain_path'), throw=True)
    if not subregion_id:
        subregion_id = get(['gridded-chain', 'run', 'spatial_filter', 'subregion_id'], domain, config.get('Paths', '_domain_path'))
        if subregion_id is not None and ',' in subregion_id:
            subregion_id = subregion_id.split(',')

    if not spatial_filter_wkt:
        spatial_filter_wkt = get(['gridded-chain', 'run', 'spatial_filter', 'wkt'], domain, config.get('Paths', '_domain_path'))
    if not spatial_filter_wkt_crs:
        spatial_filter_wkt_crs = get(['gridded-chain', 'run', 'spatial_filter', 'wkt_crs'], domain, config.get('Paths', '_domain_path'))
        if not spatial_filter_wkt_crs:
            spatial_filter_wkt_crs = dem_crs
    if not spatial_filter_wkt_buffer:
        spatial_filter_wkt_buffer = get(['gridded-chain', 'run', 'spatial_filter', 'wkt_buffer'], domain, config.get('Paths', '_domain_path'))
    if not nvstations_max:
        nvstations_max = int(get(["gridded-chain", "run", "nvstations_max"], domain, config.get('Paths', '_domain_path'))) if \
            getattribute(["gridded-chain", "run", "nvstations_max"], "enabled", domain, config.get('Paths', '_domain_path')) == "true" else None
    if config.get('General', 'distribution') == 'semi-distributed':
        dem_topoclass_bool = get(['gridded-chain', 'run', 'distribution', 'dem_topoclass_bool'], domain, config.get('Paths', '_domain_path'), throw=True)
    
    # Prepare elevation band computations
    lower_limits, upper_limits, generate_flags, band_names = get_elev_bands(domain, 'gridded-chain', cfgpath=config.get('Paths', '_domain_path'))
    
    # Load Shapefile/Geojson
    columns      = ["vstation", "easting", "northing", "lon", "lat", "elev", "i", "j", "band", "region_id"]
    ##aspects      = ["A", "A1", "A2", "A3", "A4"]
    ##aspect_names = ["flat", "north", "east", "south", "west"]
    # Setup indices
    if config.get('General', 'distribution') == 'semi-distributed':
        i_indices, j_indices, z_indices = np.meshgrid(np.arange(ds.x.size), np.arange(ds.y.size), np.arange(ds.z.size), indexing='ij')
    else:
        i_indices, j_indices = np.meshgrid(np.arange(ds.x.size), np.arange(ds.y.size), indexing='ij')
    
    # get resolution and define polygon function for cells
    dx = np.diff(ds.x.values)[0]
    dy = np.diff(ds.y.values)[0]
    if not np.isin(np.diff(ds.x.values), dx).all() or not np.isin(np.diff(ds.y.values), dy).all():
        # raise ValueError("Your DEM is irregularly spaced.")
        print("[i]  Your NWP DEM is irregularly spaced, creating spatial geometries based on provided polygons.")
        if semidistributed_polygons_path is None:
            semidistributed_polygons_path = get(["gridded-chain", "run", "distribution", "topoclass_polygons"], 
                                                domain, config.get('Paths', '_domain_path'), throw=True)
        semidistributed_polygons = gpd.read_file(semidistributed_polygons_path)
        print("     Assuming the polygons's CRS is identical to the NWP DEM CRS.")
        def create_polygon(x, y, dx=None, dy=None):
            point = Point(x, y)
            for polygon in semidistributed_polygons.geometry:
                if polygon.contains(point):
                    return polygon
            try:
                if ds[dem_topoclass_bool].sel(x=x, y=y).any():
                    print(f"[w]  Cannot find a semi-distributed polygon containing {point} but the dem_topoclass_bool requires is being modeled.")
            except KeyError:
                print(f"[w]  Issues with {point}.")
            return None
    else:
        def create_polygon(x, y, dx, dy):
            return Polygon([
                (x - dx/2, y - dy/2),
                (x + dx/2, y - dy/2),
                (x + dx/2, y + dy/2),
                (x - dx/2, y + dy/2)
            ])
    centers_x_y = [(x, y) for x, y in zip(ds.x.values[i_indices.flatten()], ds.y.values[j_indices.flatten()])]
    easting_northing = pd.DataFrame({
    'easting'   : ds.x.values[i_indices.flatten()],
    'northing'  : ds.y.values[j_indices.flatten()],
    'i'         : i_indices.flatten(),
    'j'         : j_indices.flatten(),
    'geometry'  : [create_polygon(x, y, dx, dy) for x, y in centers_x_y],
    'center'    : [Point(x, y) for x, y in centers_x_y]
    })

    # Query indexing type, i.e. data arrangement
    if len(ds.x.values) == len(ds.y.values):
        raise ValueError("The lengths of x and y coordinates are the same, which makes it ambiguous to determine the data arrangement." +
                         "Please ensure x and y have different lengths or fix the source code yourself based on an additional provided parameter.")
    if ds[var_name_surface].values.shape[:2] == (len(ds.y.values), len(ds.x.values)):
        arrangement = 'yx'
        indexing = 'xy'
    elif ds[var_name_surface].values.shape[:2] == (len(ds.x.values), len(ds.y.values)):
        arrangement = 'xy'
        indexing = 'ij'
    else:
        raise ValueError("Cannot recognize how data variables are aranged. Might it be that you still have a time coordinate in the netcdf?" +
                         "Only x, y allowed for 'fully-distributed' and x, y, z for 'semi-distributed'.")
    # Fix missing lon/lat
    if not 'lon' in ds.coords:
        x = ds['x'].values
        y = ds['y'].values
        original_crs = dem_crs
        target_crs = pyproj.CRS('EPSG:4326')
        x, y = np.meshgrid(ds['x'].values, ds['y'].values, indexing=indexing)
        transformer = pyproj.Transformer.from_crs(original_crs, target_crs, always_xy=True)
        lon_transformed, lat_transformed = transformer.transform(x, y)
        if arrangement == 'yx':
            ds = ds.assign_coords(lon=(('y', 'x'), lon_transformed))
            ds = ds.assign_coords(lat=(('y', 'x'), lat_transformed))
        elif arrangement == 'xy':
            ds = ds.assign_coords(lon=(('x', 'y'), lon_transformed))
            ds = ds.assign_coords(lat=(('x', 'y'), lat_transformed))
    if arrangement == 'yx':
        easting_northing['lon'] = ds.lon.values[j_indices, i_indices].flatten()
        easting_northing['lat'] = ds.lat.values[j_indices, i_indices].flatten()
    elif arrangement == 'xy':
        easting_northing['lon'] = ds.lon.values[i_indices, j_indices].flatten()
        easting_northing['lat'] = ds.lat.values[i_indices, j_indices].flatten()

    # Get elev and filter semi-distributed topographic classes
    if config.get('General', 'distribution') == 'semi-distributed':
        if arrangement == 'yx':
            easting_northing['elev'] = ds[var_name_surface].values[j_indices, i_indices, z_indices].flatten()
            easting_northing['true_class'] = ds[dem_topoclass_bool].values[j_indices, i_indices, z_indices].flatten()
        elif arrangement == 'xy':
            easting_northing['elev'] = ds[var_name_surface].values[i_indices, j_indices, z_indices].flatten()
            easting_northing['true_class'] = ds[dem_topoclass_bool].values[i_indices, j_indices, z_indices].flatten()
        # Filter rows that have no corresponding topographic class
        easting_northing = easting_northing[easting_northing['true_class']]
    else:
        if arrangement == 'yx':
            easting_northing['elev'] = ds[var_name_surface].values[j_indices, i_indices].flatten()
        elif arrangement == 'xy':
            easting_northing['elev'] = ds[var_name_surface].values[i_indices, j_indices].flatten()

    # Convert DataFrame to GeoDataFrame and apply spatial WKT filter
    gdf = gpd.GeoDataFrame(easting_northing, geometry='geometry', crs=dem_crs)
    if spatial_filter_wkt:
        filter_geom_user = load_wkt(spatial_filter_wkt)
        if spatial_filter_wkt_buffer:
            print("[i]  Applying a buffer to your spatial filter. Make sure you're doing this on purpose!")
            if any(geo in spatial_filter_wkt for geo in ['POLYGON', 'GEOMETRYCOLLECTION', 'MULITPOINT', 'LINESTRING']):
                print("[w]  It is generally not advised to mix usage of buffer and spatial filter WKTs that encompass multiple vstations.")
            filter_geom = filter_geom_user.buffer(float(spatial_filter_wkt_buffer))
        else: 
            filter_geom = filter_geom_user
        if dem_crs != pyproj.CRS(spatial_filter_wkt_crs):
            filter_gdf = gpd.GeoDataFrame(gpd.GeoSeries(filter_geom), columns=['geometry'], crs=pyproj.CRS(spatial_filter_wkt_crs))
            filter_gdf = filter_gdf.to_crs(dem_crs)
            filter_geom = filter_gdf.geometry.iloc[0]
            # repeat crs conversion for un-buffered user-provided geom
            filter_gdf_user = gpd.GeoDataFrame(gpd.GeoSeries(filter_geom_user), columns=['geometry'], crs=pyproj.CRS(spatial_filter_wkt_crs))
            filter_gdf_user = filter_gdf_user.to_crs(dem_crs)
            filter_geom_user = filter_gdf_user.geometry.iloc[0]
        gdf = gdf[gdf.geometry.intersects(filter_geom)]
        if gdf.empty:
            print("[E]  No grid points match search criteria - no vstations built.")
            sys.exit()
        # Calculate the distance between original filter and point geometry and sort by this distance
        gdf['distance_to_filter'] = gdf.center.apply(lambda x: filter_geom_user.distance(x))
        gdf = gdf.sort_values('distance_to_filter')
        # Write potentially updated filter to config
        config['SpatialFilter']['wkt']          = str(filter_geom)
        config['SpatialFilter']['wkt_crs']     = dem_crs.to_wkt()
        config['SpatialFilter']['wkt_buffer']   = '0'
        with open(config.get("Paths","_ini_runtime_domain"), 'w') as configfile:
            config.write(configfile)

    # Load region polygons and ensure same CRS
    polygons = gpd.read_file(shapefile_path)
    polygons = polygons.to_crs(dem_crs)
    # Ensure both GeoDataFrames have unique indices
    gdf = gdf.reset_index(drop=True)
    polygons = polygons.reset_index(drop=True)

    # Spatial join between grid points and polygons
    gdf_intersections = gpd.sjoin(gdf, polygons, how='inner', predicate='intersects')
    if gdf_intersections.empty:
        print("[E]  No grid points match search criteria - no vstations built.")
        sys.exit()
    ## Assign each cell to only one region (with most area overlap)
    # Calculate the intersection area for each pair of grid cell and region polygon
    gdf_intersections['intersection_area'] = gdf_intersections.apply(
        lambda row: row['geometry'].intersection(polygons.loc[row['index_right'], 'geometry']).area, axis=1
    )
    # Select the region polygon with the largest intersection area for each grid cell
    gdf_intersections['index_left'] = gdf_intersections.index
    gdf_intersections = gdf_intersections.reset_index(drop=True)
    idx = gdf_intersections.groupby('index_left')['intersection_area'].idxmax()
    gdf_max_intersections = gdf_intersections.loc[idx]
    # Merge the result back to the original grid cell DataFrame
    gdf = gdf.merge(gdf_max_intersections[['id', 'index_left']], left_index=True, right_on='index_left')
    gdf = gdf.reset_index(drop=True)
    gdf['region_id'] = gdf['id']

    # Filter by subregion if specified
    if subregion_id:
        gdf = gdf[np.isin(gdf['region_id'].astype(str), subregion_id)]

    # Remove rows with NaN elevation (e.g., gridpoints over ocean)
    if gdf['elev'].isna().any():
        gdf = gdf.loc[~gdf['elev'].isna(), :]

    # Add bands and filter out points where flag is not set
    gdf['band'], gdf['flag'] = find_elevation_bands(gdf['elev'], lower_limits, upper_limits, generate_flags, band_names)
    gdf = gdf[gdf['flag']]

    if nvstations_max:
        gdf = gdf.head(nvstations_max)
    if spatial_filter_wkt:
        if not (gdf['distance_to_filter'] < 10).all():
            print(f"[i]  Distance to user provided filter: [{gdf['distance_to_filter'].min()}--{gdf['distance_to_filter'].max()}] (units of your NWP DEM)")
    # Set vstation ids
    num_digits = len(str(gdf.shape[0])) + 1
    gdf['vstation'] = [(str(x) + 'A') for x in (1 + np.arange(gdf.shape[0]))]
    gdf['vstation'] = gdf['vstation'].apply(lambda x: x.zfill(num_digits))

    # Write to csv file and finish
    df_vstations = gdf[columns]
    if df_vstations.empty:
        print("[E]  No grid points match search criteria - no vstations built.")
        sys.exit()
    df_vstations.to_csv(config.get('Paths', '_vstations_csv_file'), index=False)
    print(f"[i]  File {config.get('Paths', '_vstations_csv_file')} created with {df_vstations.shape[0]} virtual stations.")


def find_elevation_bands(elevation_values, lower_limits, upper_limits, generate_flags, band_names):
    """Extract information about the elevation bands a set of elevation values fall into.

    Parameters:
    -----------
    elevation_values : np.array
        Array of elevation values for which bands need to be determined.
    lower_limits : np.array
        Array holding the lower limits of the elevation bands.
    upper_limits : np.array
        Array holding the upper limits of the elevation bands.
    generate_flags : np.array
        Array of boolean flags associated with each band.
    band_names : np.array
        Array of names associated with each elevation band.
    
    Returns:
    --------
    bands : np.array
        Array of band names where each elevation value falls into.
    flags : np.array
        Array of flags indicating whether to generate the band.
    """
    # Expand dimensions to enable broadcasting
    elevation_matrix = np.expand_dims(elevation_values, axis=1)
    lower_matrix = np.expand_dims(lower_limits, axis=0)
    upper_matrix = np.expand_dims(upper_limits, axis=0)
    
    # Create masks to find which bands each elevation value falls into
    mask = (elevation_matrix >= lower_matrix) & (elevation_matrix < upper_matrix)
    
    # Use argmax to find the first band index for which the condition is True
    indices = np.argmax(mask, axis=1)
    bands = band_names[indices]
    flags = generate_flags[indices]

    return bands, flags


if __name__ == "__main__":

    if len(sys.argv) != 2:
        sys.exit("[E] Synopsis: python3 generate_vstations.py domain")

    domain = sys.argv[1]
    generate_vstations_csv(domain)



###############################################################################################
# Examples of elevation bands given in the domain.xml file that are compatible with this module
###############################################################################################
## Tyrol
#  <elevation_bands>
#         <band>
#             <name>below_treeline</name>
#             <lower_limit>1700</lower_limit>
#             <upper_limit>2500</upper_limit>
#             <enabled>true</enabled>
#         </band>
#         <band>
#             <name>above_treeline</name>
#             <lower_limit>2500</lower_limit>
#             <upper_limit>None</upper_limit>
#             <enabled>true</enabled>
#         </band>
#  </elevation_bands>

## Norway
# <elevation_bands>
#     <band>
#         <name>0000-0300</name>
#         <lower_limit>0</lower_limit>
#         <upper_limit>300</upper_limit>
#         <enabled>true</enabled>
#     </band>
#     <band>
#         <name>0300-0600</name>
#         <lower_limit>300</lower_limit>
#         <upper_limit>600</upper_limit>
#         <enabled>true</enabled>
#     </band>
#     <band>
#         <name>0600-0900</name>
#         <lower_limit>600</lower_limit>
#         <upper_limit>900</upper_limit>
#         <enabled>true</enabled>
#     </band>
#     <band>
#         <name>0900-1200</name>
#         <lower_limit>900</lower_limit>
#         <upper_limit>1200</upper_limit>
#         <enabled>true</enabled>
#     </band>
#     <band>
#         <name>1200-1500</name>
#         <lower_limit>1200</lower_limit>
#         <upper_limit>1500</upper_limit>
#         <enabled>true</enabled>
#     </band>
#     <band>
#         <name>1500-1800</name>
#         <lower_limit>1500</lower_limit>
#         <upper_limit>1800</upper_limit>
#         <enabled>true</enabled>
#     </band>
#     <band>
#         <name>1800-2100</name>
#         <lower_limit>1800</lower_limit>
#         <upper_limit>2100</upper_limit>
#         <enabled>true</enabled>
#     </band>
# </elevation_bands>

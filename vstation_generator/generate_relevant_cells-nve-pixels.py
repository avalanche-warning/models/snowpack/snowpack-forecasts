import xarray
import shapely
import json
import pandas as pd

with open("pixels_utm.geojson") as geojson_f:
    geojson = json.load(geojson_f)
pixels = shapely.from_geojson([json.dumps(s) for s in geojson["features"]])
pixel_idx = shapely.STRtree(pixels)

nc = xarray.open_dataset("met_analysis_1_0km_Norway_20240315T21Z.nc")

db = []

for y_i, y in enumerate(nc["altitude"]):
    print(f"Processing row {y_i+1} out of {len(nc.y)}")
    for cell in y:
        x, y, z = int(cell.x.values), int(cell.y.values), int(cell.values)
        matching_pixels = pixel_idx.query(shapely.Point(x, y), "intersects")
        if len(matching_pixels) == 0:
            continue
        if len(matching_pixels) > 1:
            print(f"Multiple matching pixels for {(x, y)}")
            quit()
        elevation = f"{(z // 300 * 300):04}-{(z // 300 * 300 + 300):04}"
        pixel = matching_pixels[0]
        row = (pixel, elevation, x, y)
        db.append(row)

columns = ["pixel", "elevation", "easting", "northing"]
df = pd.DataFrame(db, columns=columns)
df.sort_values(["pixel", "elevation"], inplace=True, ignore_index=True)
df.index.rename("id", inplace=True)
df.to_csv("relevant_cells-nve-pixels.csv", sep=",")
print(f"Found {len(df.index)} relevant cells")

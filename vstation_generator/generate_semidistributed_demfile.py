import sys
import pyproj
import geopandas as gpd
import xarray as xr
import numpy as np
import rasterio
import rasterio.features
import rioxarray

from awset.domain_settings import get, getattribute, get_elev_bands

def generate_semidistributed_dem(polygons_geojson: str, 
                                 nwp_dem_netcdf: str, surface_variable: str, crs: str,
                                 elevation_band_lower_limits: np.array, elevation_band_upper_limits: np.array):
    """
    Generate a semi-distributed DEM based on polygons and NWP DEM data, 
    organized by elevation bands.

    This function computes the minimum and maximum elevation from the NWP DEM for each polygon 
    within a provided GeoJSON file, and assigns these elevations to the corresponding 
    coordinates based on the centroids of the polygons. The function then assigns 
    the polygons into different elevation bands and creates a boolean mask for each band.

    Parameters
    ----------
    polygons_geojson : str
        Path to the GeoJSON file containing polygons. Each polygon should ideally include an 'id'
    nwp_dem_netcdf : str
        Path to the NetCDF file containing NWP DEM data.
    surface_variable : str
        The surface variable (elevation data) to extract from the NetCDF data.
    crs: str
        A crs string (e.g., from pyproj.CRS.to_epsg() or similar) with the crs of the NWP DEM. 
        IMPORTANT: The crs of the polygons in the geojson needs to match the crs of the NWP DEM!
    elevation_band_lower_limits : np.array
        Array of lower limits of the elevation bands.
    elevation_band_upper_limits : np.array
        Array of upper limits of the elevation bands.

    Returns
    -------
    xr.Dataset
        A Dataset with coordinates `x`, `y`, and `z`, containing the following data variables:
        - `nwp_elevation_min`: Minimum NWP elevation for each polygon in each elevation band.
        - `nwp_elevation_max`: Maximum NWP elevation for each polygon in each elevation band.
        - `polygon_id`: Unique polygon identifier for each cell in the grid.
        - `topoclass_bool`: Boolean array indicating whether the polygon's elevation range intersects with each elevation band.
    """

    crs_object = pyproj.CRS(crs)
    # 1. Load polygons and compute centroid coordinates (x, y)
    polygons = gpd.read_file(polygons_geojson)
    centroids = polygons.geometry.centroid
    polygons['x'] = centroids.x.values
    polygons['y'] = centroids.y.values
    
    # 2. Assemble z dimension for topographical class level
    z_levels = np.sort((elevation_band_lower_limits + elevation_band_upper_limits) / 2)
    
    # 3. Create xr.Dataset with unique x, y, z coordinates
    ds = xr.Dataset(coords={'x': np.sort(polygons['x'].unique()), 'y': np.sort(polygons['y'].unique()), 'z': z_levels})

    # 2. Load NWP DEM data and extract the surface elevation
    ds_nwp = xr.open_dataset(nwp_dem_netcdf)
    nwp_elevation = ds_nwp[surface_variable]
    nwp_elevation = nwp_elevation.rio.write_crs(crs_object)  # Adjust CRS as necessary

    # 3. Compute min/max elevations for each polygon
    elev_min_grid = np.full((len(ds['x']), len(ds['y'])), np.nan)
    elev_max_grid = np.full((len(ds['x']), len(ds['y'])), np.nan)
    polygon_id_grid = np.full((len(ds['x']), len(ds['y'])), np.nan)
    
    for i, row in polygons.iterrows():
        polygon = row['geometry']
        # Create a mask for the polygon area on the DEM grid
        mask = rasterio.features.geometry_mask([polygon], 
                                               transform=nwp_elevation.rio.transform(), 
                                               invert=True, 
                                               out_shape=nwp_elevation.shape)
        # Mask the NWP elevation dataset
        masked_elev = nwp_elevation.where(mask)
        elev_min = masked_elev.min().item()
        elev_max = masked_elev.max().item()

        # Update the elev_min and elev_max grids based on the x and y indices
        x_idx = np.where(ds['x'].values == row['x'])[0][0]
        y_idx = np.where(ds['y'].values == row['y'])[0][0]
        elev_min_grid[x_idx, y_idx] = elev_min
        elev_max_grid[x_idx, y_idx] = elev_max
        if 'id' in polygons.columns:
            polygon_id_grid[x_idx, y_idx] = row['id']

    elev_min_grid_broadcast = np.repeat(elev_min_grid[:, :, np.newaxis], len(ds['z']), axis=2)
    elev_max_grid_broadcast = np.repeat(elev_max_grid[:, :, np.newaxis], len(ds['z']), axis=2)
    polygon_id_grid_broadcast = np.repeat(polygon_id_grid[:, :, np.newaxis], len(ds['z']), axis=2)
    ds['nwp_elevation_min'] = (('x', 'y', 'z'), elev_min_grid_broadcast)
    ds['nwp_elevation_max'] = (('x', 'y', 'z'), elev_max_grid_broadcast)
    ds['polygon_id'] = (('x', 'y', 'z'), polygon_id_grid_broadcast)

    # 7. Create topoclass_bool variable based on elevation bands
    topoclass_bool = np.full((len(ds['x']), len(ds['y']), len(ds['z'])), np.nan).astype(bool)
    for lower_limit, upper_limit, zz, z_i in zip(elevation_band_lower_limits, elevation_band_upper_limits, ds['z'], range(len(ds['z']))):
        topoclass_bool[:, :, z_i] = (
            (ds['nwp_elevation_min'][:, :, z_i] < upper_limit) &
            (ds['nwp_elevation_max'][:, :, z_i] >= lower_limit)
        )
    ds['topoclass_bool'] = (('x', 'y', 'z'), topoclass_bool)

    # 8. Add 'level' data variable to represent elevation band levels
    level = np.broadcast_to(ds['z'].values, ds['topoclass_bool'].shape)
    ds['level'] = (('x', 'y', 'z'), level)

    print(f"[i]  Setting CRS attribute of resulting semi-distributed DEM as wkt string (Input CRS: {crs}). The crs of the provided polygons needs to be the same!")
    ds = ds.assign_attrs(crs=crs_object.to_wkt())
    ds = ds.assign_attrs(crs_proj4=crs_object.to_proj4())
   
    return ds

if __name__ == "__main__":

    if len(sys.argv) != 6:
        sys.exit("[E] Synopsis: python3 generate_semidistributed_demfile.py domain path/to/nwp/dem sfc_variable nwp_crs path/to/outfile")

    domain = sys.argv[1]
    if getattribute(['gridded-chain', 'run', 'distribution'], 'type', domain) != 'semi-distributed':
        sys.exit("[E] This script is intended to be used for semi-distributed modeling configurations only. Set gridded-chain/run/distribution type=semi-distributed first!")
    polygons_geojson = get(["gridded-chain", "run", "distribution", "topoclass_polygons"], domain)
    nwp_dem_netcdf = sys.argv[2]
    surface_variable = sys.argv[3]
    crs = sys.argv[4]
    lower_limits, upper_limits, generate_flags, band_names = get_elev_bands(domain, 'gridded-chain')

    ds = generate_semidistributed_dem(polygons_geojson, 
                                    nwp_dem_netcdf, surface_variable, crs,
                                    elevation_band_lower_limits=lower_limits[generate_flags], 
                                    elevation_band_upper_limits=upper_limits[generate_flags])
    outpath = sys.argv[5]
    print(f"[i]  Saving {outpath}")
    ds.to_netcdf(outpath)
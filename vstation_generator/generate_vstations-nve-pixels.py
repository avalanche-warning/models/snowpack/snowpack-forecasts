import xarray
import shapely
import json
import pandas as pd
from pyproj import Transformer

UTM33_2_WGS84 = Transformer.from_crs("EPSG:25833", "EPSG:4326")

with open("pixels_utm.geojson") as geojson_f:
    json_pixels = json.load(geojson_f)
pixels = shapely.from_geojson([json.dumps(s) for s in json_pixels["features"]])
with open("forecasting_regions_awsome_utm.geojson") as geojson_f:
    json_regions = json.load(geojson_f)
regions = shapely.from_geojson([json.dumps(s) for s in json_regions["features"]])
region_ids = {
    idx: region["properties"]["omrID"]
    for idx, region in enumerate(json_regions["features"])
}
region_idx = shapely.STRtree(regions)

relevant_cells = pd.read_csv("relevant_cells-nve-pixels.csv", index_col="id")
pixel_elevs = relevant_cells.groupby(["pixel", "elevation"]).size().to_dict()

db = {}
for pixel_id, pixel in enumerate(pixels):
    print(f"Processing pixel {pixel_id+1} out of {len(pixels)}")
    matching_regions = region_idx.query(pixel, "intersects")
    if len(matching_regions) == 0:
        region_id = -999
    else:
        max_area, max_id = None, None
        for region_i in matching_regions:
            intersection = shapely.intersection(pixel, regions[region_i])
            area = shapely.area(intersection)
            if max_area is None or max_area < area:
                max_area = area
                max_i = region_i
        region_id = region_ids[max_i]

    centroid = shapely.centroid(pixel)
    lat, lon = UTM33_2_WGS84.transform(centroid.x, centroid.y)

    for z in range(6):
        elev_bottom = z * 300
        elev_center, elev_top = elev_bottom + 150, elev_bottom + 300
        elev_str = f"{(elev_bottom):04}-{(elev_top):04}"
        if (pixel_id, elev_str) not in pixel_elevs:
            continue

        db[f"{pixel_id:06}{elev_bottom:04}A"] = {
            "lon": f"{lon:.6f}",
            "lat": f"{lat:.6f}",
            "elev": elev_center,
            "band": elev_str,
            "region_id": region_id,
            "n_cells": pixel_elevs[(pixel_id, elev_str)]
        }

df = pd.DataFrame(db).T
df.index.name = "vstation"
df.to_csv("vstations.csv", sep=",")
